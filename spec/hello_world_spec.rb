require 'spec_helper'

RSpec.describe "#print" do
  subject { HelloWorld.new }

  it "return hello world" do
    expect(subject.print).to eq("Hello World!")
  end
end
